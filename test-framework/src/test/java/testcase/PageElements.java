package testcase;

import java.util.Arrays;
import java.util.List;

/**
 * Urls and selectors.
 * 
 * @author rm
 *
 */
public class PageElements {

	// Basic page information
	public static final String BASE_URL = "http://www.bolelut.pl";
	public static final String PAGE_TITLE = "Bolelut";
	public static final int ANSWER_FADEOUT_TIMEOUT = 7;
	
	// Selectors
	public static final String TITLE_BANNER = "#header";
	public static final String QUERY_FIELD = "#question";
	public static final String SUBMIT_BUTTON = "#submit";
	public static final String ANSWER = "#answer";
	
	// Expected answers
	public static final String NO_QUESTION_MARK_ANSWER = "You call this a question? It doesn't even have a question mark.";
	public static final String EMPTY_QUESTION_ANSWER = "I'm sorry, I can't read your mind. Type in a question.";
	public static final List<String> ANSWERS = Arrays.asList(
			"Can you clarify?",
		    "Sorry, I didn't catch that, can you repeat?",
		    "What was that again?",
		    "Don't you have other problems?",
		    "This sounds like a first-world problem to me.",
		    "How should I know?",
		    "People around the world are dying or starving, and you come to me with this??",
		    "I think I saw someone behind your back.",
		    "Watching you type, it's like time stood still.",
		    "Are you kidding me?",
		    "Oh please, I've heard this question so many times, it makes me sick.",
		    "I'm a highly sophisticated AI, not an oracle dammit.",
		    "Didn't you learn this at school?",
		    "What? You humans sometimes make me really sad.",
		    "Sorry, my mom called. What was that again?"
			);
}
