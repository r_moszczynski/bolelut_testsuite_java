package testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Utility methods.
 * 
 * @author rm
 *
 */
public class Utilities {
	
	/**
	 * Open the main page.
	 * 
	 * @param url 		URL of the main page
	 * @param driver 	browser instance
	 */
	public static void openBaseUrl(String url, WebDriver driver) {
		driver.get(url);
	}
	
	/**
	 * Create a custom expected condition for WebDriverWait.
	 * The condition to wait for is finishing all jQuery animations.
	 * 
	 * @return expected condition
	 */
	public static ExpectedCondition<Boolean> isAnimationFinished() {
		return new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return !(Boolean) ((JavascriptExecutor) driver).executeScript("return $('#panel').is(':animated');");
			}
		};
	}
	
	/**
	 * Wrapper for waiting until jQuery animations are finished.
	 * 
	 * @param driver	browser instance
	 */
	public static void waitUntilAnimationFinished(WebDriver driver) {
		WebDriverWait w = new WebDriverWait(driver, 3);
		w.until(isAnimationFinished());
	}
	
	/**
	 * Toggle visibility of the query field.
	 * 
	 * @param driver	browser instance
	 */
	public static void toggleQueryField(WebDriver driver) {
		driver.findElement(By.cssSelector(PageElements.TITLE_BANNER)).click();
		Utilities.waitUntilAnimationFinished(driver);
	}
	
	/** 
	 * Open the query field and submit a query.
	 * 
	 * @param query		query string
	 * @param driver	browser instance
	 */
	public static void submitQuery(String query, WebDriver driver) {
		toggleQueryField(driver);
		
		WebElement queryField = driver.findElement(By.cssSelector(PageElements.QUERY_FIELD));
		WebElement submitButton = driver.findElement(By.cssSelector(PageElements.SUBMIT_BUTTON));
		
		queryField.clear();
		queryField.sendKeys(query);
		submitButton.click();
	}
}
