package testcase;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

/**
 * Parent class for creating test cases.
 * 
 * @author rm
 *
 */
@RunWith(Parameterized.class)
public abstract class TestCase {
	
	protected WebDriver driver = null;
	
	public TestCase(WebDriver driver) {
		this.driver = driver;
	}
	
	@Parameters
	public static List<Object> browsers() {
        return Arrays.asList(new Object[] {
        		new FirefoxDriver(),
        		new PhantomJSDriver()
          });
	}
	
	@Before
	public void setUp() {
		driver.manage().window().maximize();
		Utilities.openBaseUrl(PageElements.BASE_URL, driver);
	}
	
	@After
	public void tearDown() {
		driver.close();
	}
}
