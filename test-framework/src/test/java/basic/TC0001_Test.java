package basic;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import testcase.PageElements;
import testcase.TestCase;

/**
 * TC0001 
 * 
 * Verify that page title is correct.
 * 
 * @author rm
 *
 */
public class TC0001_Test extends TestCase {

	public TC0001_Test(WebDriver driver) {
		super(driver);
	}

	@Test
	public void runTest() {
		String title = driver.getTitle();
		Assert.assertEquals("Unexpected title found.", PageElements.PAGE_TITLE, title);
	}
}
