package basic;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import testcase.PageElements;
import testcase.TestCase;
import testcase.Utilities;

/**
 * TC0007
 * 
 * Verify that answers disappears after a configured amount of time.
 * 
 * @author rm
 *
 */
public class TC0007_Test extends TestCase {

	public TC0007_Test(WebDriver driver) {
		super(driver);
	}

	private final String QUERY = "What is the meaning of the Universe?";
	
	@Test
	public void runTest() {
		Utilities.submitQuery(QUERY, driver);
		By answerSelector = By.cssSelector(PageElements.ANSWER);

		WebDriverWait w = new WebDriverWait(driver, PageElements.ANSWER_FADEOUT_TIMEOUT);
		
		try {
			w.until(ExpectedConditions.invisibilityOfElementLocated(answerSelector));
		} catch (Exception e) {
			Assert.fail("Answer did not disappear as expected.");
		}
	}
}
