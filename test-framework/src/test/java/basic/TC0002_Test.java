package basic;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import testcase.PageElements;
import testcase.TestCase;
import testcase.Utilities;

/**
 * TC0002
 * 
 * Verify that query field and submit button appear after clicking the banner.
 * 
 * @author rm
 *
 */
public class TC0002_Test extends TestCase {

	public TC0002_Test(WebDriver driver) {
		super(driver);
	}

	@Test
	public void runTestField() {
		Utilities.toggleQueryField(driver);
		
		Assert.assertTrue("Query field was not displayed.",
				driver.findElement(By.cssSelector(PageElements.QUERY_FIELD))
						.isDisplayed());
	}

	@Test
	public void runTestButton() {
		Utilities.toggleQueryField(driver);
		
		Assert.assertTrue("Submit button was not displayed.", driver
				.findElement(By.cssSelector(PageElements.SUBMIT_BUTTON))
				.isDisplayed());
	}
}
