package basic;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import testcase.PageElements;
import testcase.TestCase;
import testcase.Utilities;

/**
 * TC0005
 * 
 * Verify that correct response is returned for an empty question.
 * 
 * @author rm
 *
 */
public class TC0005_Test extends TestCase {

	public TC0005_Test(WebDriver driver) {
		super(driver);
	}

	private final String QUERY = "";

	@Test
	public void runTest() {
		Utilities.submitQuery(QUERY, driver);
		WebElement answer = driver
				.findElement(By.cssSelector(PageElements.ANSWER));

		Assert.assertEquals("Wrong answer returned.",
				PageElements.EMPTY_QUESTION_ANSWER, answer.getText());
	}
}
