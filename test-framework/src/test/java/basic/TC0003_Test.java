package basic;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import testcase.PageElements;
import testcase.TestCase;
import testcase.Utilities;

/**
 * TC0003
 * 
 * Verify that query field and button disappear after clicking the banner twice.
 * 
 * @author rm
 *
 */
public class TC0003_Test extends TestCase {

	public TC0003_Test(WebDriver driver) {
		super(driver);
	}

	@Test
	public void runTestField() {
		Utilities.toggleQueryField(driver);
		Utilities.toggleQueryField(driver);
		
		Assert.assertFalse("Query field is displayed.",
				driver.findElement(By.cssSelector(PageElements.QUERY_FIELD))
						.isDisplayed());
	}

	@Test
	public void runTestButton() {
		Utilities.toggleQueryField(driver);
		Utilities.toggleQueryField(driver);
		
		Assert.assertFalse("Submit is displayed.", driver
				.findElement(By.cssSelector(PageElements.SUBMIT_BUTTON))
				.isDisplayed());
	}
}
