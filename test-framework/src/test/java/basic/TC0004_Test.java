package basic;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import testcase.PageElements;
import testcase.TestCase;
import testcase.Utilities;

/**
 * TC0003
 * 
 * Verify that correct response is returned for questions without a question
 * mark.
 * 
 * @author rm
 *
 */
public class TC0004_Test extends TestCase {

	public TC0004_Test(WebDriver driver) {
		super(driver);
	}

	private final String QUERY = "What is the meaning of the Universe";

	@Test
	public void runTest() {
		Utilities.submitQuery(QUERY, driver);
		WebElement answer = driver
				.findElement(By.cssSelector(PageElements.ANSWER));

		Assert.assertEquals("Wrong answer returned.",
				PageElements.NO_QUESTION_MARK_ANSWER, answer.getText());
	}
}
