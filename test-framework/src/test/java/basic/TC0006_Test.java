package basic;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import testcase.PageElements;
import testcase.TestCase;
import testcase.Utilities;

/**
 * TC0006
 * 
 * Verify that a response from a predefined list is returned for a correctly
 * formulated question.
 * 
 * @author rm
 *
 */
public class TC0006_Test extends TestCase {

	public TC0006_Test(WebDriver driver) {
		super(driver);
	}

	private final String QUERY = "What is the meaning of the Universe?";

	@Test
	public void runTest() {
		Utilities.submitQuery(QUERY, driver);
		WebElement answer = driver
				.findElement(By.cssSelector(PageElements.ANSWER));

		Assert.assertTrue("Unexpected answer returned.",
				PageElements.ANSWERS.contains(answer.getText()));
	}
}
